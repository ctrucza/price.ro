﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PriceRo
{
    public class ProductDesignRepository1 : INotifyPropertyChanged
    {
        public ProductDesignRepository1()
        {
            if (DesignerProperties.IsInDesignTool)
            {
                Products = new ObservableCollection<Product>
                {
                    new Product { Name = "Name1", imageUrl = "http://i2.best-price.ro/images/preturi/small_new/4/8/0/738480.jpg", Price="486.28 LEI", ButtonNo="23" },
                    new Product { Name = "Name2", imageUrl = "http://i2.best-price.ro/images/preturi/small_new/4/8/0/738480.jpg", Price="86.28 LEI", ButtonNo="2" },
                    new Product { Name = "Name3", imageUrl = "http://i2.best-price.ro/images/preturi/small_new/4/8/0/738480.jpg", Price="386.28 LEI", ButtonNo="1" },
                    new Product { Name = "Name4", imageUrl = "http://i2.best-price.ro/images/preturi/small_new/4/8/0/738480.jpg", Price="1986.28 LEI" }
                };
            }
        }

        private ObservableCollection<Product> _products;
        public ObservableCollection<Product> Products
        {
            get { return _products; }
            set { _products = value; NotifyOfPropertyChanged("Products"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }

    public class Product : INotifyPropertyChanged
    {
        private string _name;
        private string _specs;
        private string _imageUrl;
        private string _price;
        private string _url;
        private string _ButtonNo;
        private string _SpecsUrl;

        public string imageUrl { get { return _imageUrl; } set { _imageUrl = value; NotifyOfPropertyChanged("imageUrl"); } }
        public string Name { get { return _name; } set { _name = value; NotifyOfPropertyChanged("Name"); } }
        public string Specifications { get { return _specs; } set { _specs = value; NotifyOfPropertyChanged("Specifications"); } }
        public string Price { get { return _price; } set { _price = value; NotifyOfPropertyChanged("Price"); } }
        public string Url { get { return _url; } set { _url = value; NotifyOfPropertyChanged("Url"); } }
        public string ButtonNo { get { return _ButtonNo; } set { _ButtonNo = value; NotifyOfPropertyChanged("ButtonNo"); } }
        public string SpecsUrl { get { return _SpecsUrl; } set { _SpecsUrl = value; NotifyOfPropertyChanged("SpecsUrl"); } }
        public List<Market> markets  = new List<Market>();


        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
