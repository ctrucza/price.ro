﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using HtmlAgilityPack;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Threading;

namespace PriceRo
{
    public partial class MainPage : PhoneApplicationPage, INotifyPropertyChanged
    {
        private BackgroundWorker backroungWorker;
        Popup popup;
        string MenuSearch;
        HtmlWeb webPage = new HtmlWeb();

        string Url;
        private ObservableCollection<Product> _products;
        public ObservableCollection<Product> Products
        {
            get { return _products; }
            set { _products = value; NotifyOfPropertyChanged("Products"); }
        }
        
        
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DataContext = this;

            webPage.LoadCompleted += webPage_LoadCompleted;
            webPage.LoadAsync(Url);
            
        }

        public void webPage_LoadCompleted(object sender, HtmlDocumentLoadCompleted e)
        {
            HtmlDocument document = e.Document;
            HtmlNode tableNode = document.DocumentNode.SelectSingleNode("//table[@id='effortTable']");

            List<Product> products = new List<Product>();
            int index = 1;
            string productIndex = "button" + Convert.ToString(index);
            

            if (document.DocumentNode != null)
            {
                //Select each product from the table list
                foreach (var node in tableNode.SelectNodes("//tr[@class='trList']"))
                {
                    Product newProduct = new Product();

                    var childs = node.SelectNodes("td");

                    var img = childs[0].SelectSingleNode("a/img");
                    var title = childs[1].SelectSingleNode("a").InnerText;
                    var prodSpec = HtmlEntity.DeEntitize(childs[1].SelectSingleNode("br").NextSibling.InnerText).Trim();
                    var prodPrice = childs[2].SelectSingleNode("a[@class='pret']").InnerText;
                    var prodUrl = childs[2].SelectSingleNode("a");

                    newProduct.Name = title;
                    newProduct.Specifications = prodSpec;
                    newProduct.Price = prodPrice;
                    newProduct.Url = prodUrl.Attributes["href"].Value;
                    
                    productIndex = "button" + Convert.ToString(index);
                    newProduct.ButtonNo = productIndex;

                    index += 1;

                    if (img == null)
                        newProduct.imageUrl = null;
                    else
                    {
                        newProduct.imageUrl = img.Attributes["src"].Value;

                    }

                    //var productName = ;
                    //var productSpecs = ;
                    //var productPrice = ;
                    // string finalNode= node.InnerText.Trim(' ', '\t', '\n', '\r').
                    // Replace("\n", string.Empty).Replace("\r", string.Empty);

                    products.Add(newProduct);
                    
                }
            }

            Products = new ObservableCollection<Product>(products);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            MenuSearch = HttpUtility.UrlDecode(NavigationContext.QueryString["search"]);
            Url = "http://www.price.ro/index.php?action=q&text=" + MenuSearch + "&categ_name=toate+categoriile&categ=&x=0&y=0&model=&model_not=&producer_name=&producer_name_not=&details=&details_not=";
            webPage.LoadCompleted += webPage_LoadCompleted;

            webPage.LoadAsync(Url);
        }
        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            string newString;

            newString = searchBar.Text.Replace(" ", "+");


            string finalString = "http://www.price.ro/index.php?action=q&text=" + newString +"&categ_name=toate+categoriile&categ=&x=0&y=0&model=&model_not=&producer_name=&producer_name_not=&details=&details_not=";
            Url = finalString;

            webPage.LoadCompleted += webPage_LoadCompleted;
            
            webPage.LoadAsync(Url);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Product currentProduct = new Product();

            foreach (var prod in Products)
            {
                Button currBtn = (Button) sender;
                if (prod.ButtonNo == currBtn.Tag.ToString())
                {
                    currentProduct = prod;
                    break;
                }
            }

            string name = HttpUtility.UrlEncode(currentProduct.Name);
            string price =  HttpUtility.UrlEncode(currentProduct.Price);
            string imgUrl =  HttpUtility.UrlEncode(currentProduct.imageUrl);
            string Url =  HttpUtility.UrlEncode(currentProduct.Url);
            string specs =  HttpUtility.UrlEncode(currentProduct.Specifications);

            NavigationService.Navigate(new Uri("/SecondPage.xaml?objName=" + name + "&objSpecs=" + specs + "&objPrice=" + price + "&objImgUrl=" + imgUrl + "&objUrl=" + Url, UriKind.Relative));

        }


    }
}