﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Threading;
namespace PriceRo
{
    public partial class Page1 : PhoneApplicationPage
    {
        private BackgroundWorker backroungWorker;
        Popup popup; 

        public Page1()
        {
            InitializeComponent();

            ShowSplash();
        }

        private void ShowSplash()
        {
            this.popup = new Popup();
            this.popup.Child = new SplashScreenControl();
            this.popup.IsOpen = true;
            StartLoadingData();
        }


        private void StartLoadingData()
        {
            backroungWorker = new BackgroundWorker();
            backroungWorker.DoWork += new DoWorkEventHandler(backroungWorker_DoWork);
            backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);
            backroungWorker.RunWorkerAsync();
        }
        void backroungWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                this.popup.IsOpen = false;

            }
            );
        }
        void backroungWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // call service here .
            Thread.Sleep(1000);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Tag = SearchBox.Text;
            string searchContent = HttpUtility.UrlEncode(SearchBox.Text);

            NavigationService.Navigate(new Uri("/MainPage.xaml?search=" + searchContent, UriKind.Relative));
        }

        private void OpenWindowAbout(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/YourLastAboutDialog;component/AboutPage.xaml", UriKind.Relative));
        }
    }
}