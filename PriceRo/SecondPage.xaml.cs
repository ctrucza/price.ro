﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using HtmlAgilityPack;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace PriceRo
{
    public partial class SecondPage : PhoneApplicationPage, INotifyPropertyChanged
    {
        private Product _currentProduct = new Product();

        public Product currentProduct
        {
            get { return _currentProduct; }
            set
            {
                _currentProduct = value;
                NotifyOfPropertyChanged("currentProduct");
            }
        }

        public string Url;

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            currentProduct.Name = HttpUtility.UrlDecode(NavigationContext.QueryString["objName"]);
            currentProduct.Specifications = HttpUtility.UrlDecode(NavigationContext.QueryString["objSpecs"]);
            currentProduct.Price = HttpUtility.UrlDecode(NavigationContext.QueryString["objPrice"]);
            currentProduct.Url = HttpUtility.UrlDecode(NavigationContext.QueryString["objUrl"]);
            currentProduct.imageUrl = HttpUtility.UrlDecode(NavigationContext.QueryString["objImgUrl"]);
            Url = "http://www.price.ro" + currentProduct.Url;


            HtmlWeb webPage = new HtmlWeb();
            webPage.LoadCompleted += webPage_LoadCompleted;
            webPage.LoadAsync(Url);
        }


        public SecondPage()
        {
            InitializeComponent();

            DataContext = this;
        }

        private ObservableCollection<Market> _markets;
        public ObservableCollection<Market> Markets
        {
            get { return _markets; }
            set { _markets = value; NotifyOfPropertyChanged("Markets"); }
        }



        public void webPage_LoadCompleted(object sender, HtmlDocumentLoadCompleted e)
        {

            HtmlDocument document = e.Document;
            HtmlNode tableNode = document.DocumentNode.SelectSingleNode("//table[@class='text_11' and @bordercolor='#FFFFFF']");

            List<Market> markets = new List<Market>();

            //Select Specifications Url of Current Product
            currentProduct.SpecsUrl = "http://www.price.ro" + document.DocumentNode.SelectSingleNode("//td[@class='text_11' and @valign='top' and @colspan='2']/a").Attributes["href"].Value;

            if (document.DocumentNode != null && tableNode != null)
            {
                //Select each product from the table list
               foreach (var node in tableNode.SelectNodes("//tr[@class='trList']"))
                {
                        Market market = new Market();

                        var childs = node.SelectNodes("td");
                         
                        market.Availability = childs[0].SelectSingleNode("span").InnerText;
                        market.Price = childs[1].SelectSingleNode("a").InnerText;
                        var halfPrice = market.Price.Split(' ');
                        market.intPrice = float.Parse(halfPrice[0]);
                        market.lastTime = childs[2].InnerText;
                        
                        string marketName = null;
                        var checkValidity = childs[4].SelectSingleNode("a/img");
                        if (checkValidity != null)
                        {
                            marketName = childs[4].SelectSingleNode("a/img").Attributes["alt"].Value;
                            market.picUrl = childs[4].SelectSingleNode("a/img").Attributes["src"].Value;
                        }
                        else
                        {
                            marketName = childs[4].SelectSingleNode("table//a").Attributes["title"].Value;
                        }
                        var splittedMarketName = marketName.Split(' ');

                        if (childs[4].SelectSingleNode("a") != null)
                        {
                            market.Url = childs[4].SelectSingleNode("a").Attributes["href"].Value;
                        }
                        else
                        {
                            market.Url = childs[4].SelectSingleNode("table//a").Attributes["href"].Value;
                        }
                        
                        string marketUrl = market.Url;
                        var splittedJavaScriptLink = marketUrl.Split(new Char[] { '\''});

                        market.Url = string.Format("http://www.price.ro/preturi/referal_product.php?store_id={0}&prod_id={1}&pret_id={2}",
                                                    splittedJavaScriptLink[1], splittedJavaScriptLink[3], splittedJavaScriptLink[5]);
                        splittedMarketName[0] = "";
                        splittedMarketName[1] = "";
                        splittedMarketName[2] = "";
                        splittedMarketName[3] = "";
                        splittedMarketName[4] = "";

                       foreach(var stringx in splittedMarketName)
                       {
                           market.Name = market.Name + stringx + " ";
                       }

                        markets.Add(market);
                 
                }
            }
            currentProduct.markets = markets;
            Markets = new ObservableCollection<Market>(markets);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button currBtn = (Button)sender;

            WebBrowserTask wbt = new WebBrowserTask();
            wbt.URL = currBtn.Tag.ToString();
            wbt.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string EncodedSpecsUrl = HttpUtility.UrlEncode(currentProduct.SpecsUrl);

            NavigationService.Navigate(new Uri("/Specifications.xaml?specsUrl=" + EncodedSpecsUrl, UriKind.Relative));
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            Markets = new ObservableCollection<Market>(Markets.OrderByDescending(m => m._intPrice));
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            Markets = new ObservableCollection<Market>(Markets.OrderBy(m => m._intPrice));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        
    }
}