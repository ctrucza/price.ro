﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using HtmlAgilityPack;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PriceRo
{
    public partial class Specifications : PhoneApplicationPage, INotifyPropertyChanged
    {
        public string _allSpecs;

        public string allSpecs
        {
            get
            {
                return _allSpecs;
            }
            set
            {
                _allSpecs = value;
                NotifyOfPropertyChanged("allSpecs");
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string ourSpecsUrl = HttpUtility.UrlDecode(NavigationContext.QueryString["specsUrl"]);

            HtmlWeb webPage = new HtmlWeb();
            webPage.LoadCompleted += webPage_LoadCompleted;
            webPage.LoadAsync(ourSpecsUrl);

        }

        public Specifications()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void webPage_LoadCompleted(object sender, HtmlDocumentLoadCompleted e)
        {

            HtmlDocument document = e.Document;
            HtmlNode tableNode = document.DocumentNode.SelectSingleNode("//td[@class='text_11' and @width='80%' and @valign='top']");

            if (document.DocumentNode != null && tableNode != null)
            {
                //Select each product from the table list
                foreach (var child in tableNode.ChildNodes)
                {
                   // allSpecs = allSpecs + child.InnerText;
                    allSpecs = tableNode.InnerText;
                    specificatii_Web.NavigateToString(allSpecs);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void WebBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {

        }
    }
}