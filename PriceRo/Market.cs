﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PriceRo
{
    public class ProductDesignRepository : INotifyPropertyChanged
    {
        public ProductDesignRepository()
        {
            if (DesignerProperties.IsInDesignTool)
            {
                Markets = new ObservableCollection<Market>
                {
                    new Market { Name = "Intend Computers", Price ="200.92 LEI" , lastTime ="Acum 2 minute", Availability = "In stoc", picUrl = "http://i1.best-price.ro/images/store/logo_1175.jpg" },
                    new Market { Name = "Intend Computers", Price ="200.92 LEI" , lastTime ="Acum 2 minute", Availability = "In stoc"},
                    new Market { Name = "Intend Computers", Price ="200.92 LEI" , lastTime ="Acum 2 minute", Availability = "In stoc"},
                    new Market { Name = "Intend Computers", Price ="200.92 LEI" , lastTime ="Acum 2 minute", Availability = "In stoc"},
                    new Market { Name = "Intend Computers", Price ="200.92 LEI" , lastTime ="Acum 2 minute", Availability = "In stoc"}
                };
            }
        }

        private ObservableCollection<Market> _markets;
        public ObservableCollection<Market> Markets
        {
            get { return _markets; }
            set { _markets = value; NotifyOfPropertyChanged("Markets"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
    public class Market
    {
        public string _name;
        public string _availability;
        public string _price;
        public string _url;
        public string _lastTime;
        public string _picUrl;
        public float _intPrice;

        public string Name { get { return _name; } set { _name = value; NotifyOfPropertyChanged("Name"); } }
        public string Availability { get { return _availability; } set { _availability = value; NotifyOfPropertyChanged("Availability"); } }
        public string Price { get { return _price; } set { _price = value; NotifyOfPropertyChanged("Price"); } }
        public string Url { get { return _url; } set { _url = value; NotifyOfPropertyChanged("Url"); } }
        public string lastTime { get { return _lastTime; } set { _lastTime = value; NotifyOfPropertyChanged("lastTime"); } }
        public string picUrl { get { return _picUrl; } set { _picUrl = value; NotifyOfPropertyChanged("picUrl"); } }
        public float intPrice { get { return _intPrice; } set { _intPrice = value; NotifyOfPropertyChanged("intPrice"); } }

        protected void NotifyOfPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
